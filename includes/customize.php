<?php

function netsby_customize_register( $wp_customize ) {
	$wpml = NETSBY['wpml'];

	// Sections
	$wp_customize->add_section( 'netsby_theme', array(
		'title' => esc_html__( 'Netsby', 'netsby' ),
	) );

	// Settings
	$wp_customize->add_setting( 'block_editor_enable', array(
		'default'           => false,
		'sanitize_callback' => 'netsby_sanitize_checkbox',
	) );

	$wp_customize->add_setting( 'site_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_setting( 'build_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );

	// Controls
	$wp_customize->add_control( 'gutenberg', array(
		'label'    => esc_html__( 'Enable Gutenberg', 'netsby' ),
		'section'  => 'netsby_theme',
		'settings' => 'block_editor_enable',
		'type'     => 'checkbox',
	) );

	$wp_customize->add_control( 'redirect', array(
		'label'       => esc_html__( 'Redirect', 'netsby' ),
		'description' => esc_html__( 'Domain of the static site. Permalinks will be updated accordingly.', 'netsby' ),
		'section'     => 'netsby_theme',
		'settings'    => 'site_url',
		'type'        => 'url',
	) );
	$wp_customize->add_control( 'build', array(
		'label'       => esc_html__( 'Build', 'netsby' ),
		'description' => esc_html__( 'Webhook URL to trigger new builds.', 'netsby' ),
		'section'     => 'netsby_theme',
		'settings'    => 'build_url',
		'type'        => 'url',
	) );

	// WPML
	if ( ! empty( $wpml ) ) {
		if ( $wpml['lang_dirs'] ) {
			$wp_customize->add_setting( 'lang_default_dir', array(
				'default'           => false,
				'sanitize_callback' => 'netsby_sanitize_checkbox',
			) );

			$wp_customize->add_control( 'lang_default_dir', array(
				'label'    => esc_html__( '[WPML] Use directory for default language', 'netsby' ),
				'section'  => 'netsby_theme',
				'settings' => 'lang_default_dir',
				'type'     => 'checkbox',
			) );
		}
	}
}

// Sanitization
function netsby_sanitize_checkbox( $input ) {
	// Return true if checkbox is checked.
	return ( isset( $input ) && true === $input ) ? true : false;
}
