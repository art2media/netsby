<?php

function netsby_setup_theme() {
	$template_dir = NETSBY['dir'];

	load_theme_textdomain( 'netsby', $template_dir . '/languages' );
}

// Create secret token on admin page load, if user is administrator.
// But only if empty or 'netsby_secret' is set as url param.
function netsby_token_secret() {
	$token_secret = get_theme_mod( 'token_secret' );

	if ( is_admin() && current_user_can( 'administrator' ) && ( empty( $token_secret ) || isset( $_GET['netsby_secret'] ) ) ) :
		$token = netsby_token( true ); ?>

	<div class="notice notice-success is-dismissible">
		<p><?php _e( 'Copy this secret access token somewhere safe:', 'netsby' ); ?></p>

		<p><code><?php echo esc_html( $token ); ?></code></p>
	</div>

	<?php
	endif;
}
