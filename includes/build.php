<?php

function netsby_build() {
	if ( defined('DOING_AJAX') && DOING_AJAX ) {
		$build_url = $_POST['build_url'];

		if ( ! empty( $build_url ) ) {
			// Send outgoing Webhook for a new build.
			wp_remote_post( $build_url );

			// Incoming Webhook needs a while. Update the status manually.
			set_theme_mod( 'build_status', 'building' );
		}

		// Always die after AJAX request.
		wp_die();
	}
}

function netsby_build_status( $data = '' ) {
	if ( ! empty( $data ) ) {
		// Set updated build status from incoming Webhook.
		// Status may be 'building', 'ready' or 'failed'.
		set_theme_mod( 'build_status', $data['state'] );
	}

	// Get current build status during AJAX refresh.
	if ( defined('DOING_AJAX') && DOING_AJAX ) {
		// Get non-cached build status.
		$build_status = get_theme_mod( 'build_status' );

		if ( ! empty( $build_status ) ) {
			// Always JSON encode response data.
			echo json_encode( $build_status );
		}

		// Always die after AJAX request.
		wp_die();
	}
}

function netsby_build_unlock() {
	// Unlock build status by manually setting it to ready.
	if ( defined('DOING_AJAX') && DOING_AJAX ) {
		set_theme_mod( 'build_status', 'ready' );

		// Always die after AJAX request.
		wp_die();
	}
}
