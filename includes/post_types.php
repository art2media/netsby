<?php

// Rewrite permalinks to match static site structure.
function netsby_permalinks( $permalink ) {
	$site_url = NETSBY['mods']['site_url'];
	$lang_dir = NETSBY['mods']['lang_default_dir'];
	$wpml     = NETSBY['wpml'];

	if ( ! empty( $site_url ) ) {
		$link_path    = netsby_link_path( $permalink );
		$prepend_path = '';

		// Prepend link path with default language, if:
		// - WPML plugin is active.
		// - Theme mod for directory of default language is set.
		// - Setting for language directories is enabled.
		// - Current language equals default language.
		if ( ! empty( $wpml ) && $lang_dir && $wpml['lang_dirs'] && ( $wpml['lang_default'] === $wpml['lang_current'] ) ) {
			$prepend_path .= '/' . $wpml['lang_default'];
		}

		return untrailingslashit( $site_url ) . $prepend_path . $link_path;
	}

	return $permalink;
}

// Register custom post types.
function netsby_cpt() {
	$mails = get_posts( array(
		'numberposts' => 1,
		'post_type'   => 'mail',
		'post_status' => 'private',
	) );

	register_post_type( 'mail', array(
		'labels' => array(
			'name'                     => __( 'Mails', 'netsby' ),
			'singular_name'            => __( 'Mail', 'netsby' ),
			'edit_item'                => __( 'Edit Mail', 'netsby' ),
			'view_item'                => __( 'View Mail', 'netsby' ),
			'view_items'               => __( 'View Mails', 'netsby' ),
			'search_items'             => __( 'Search Mails', 'netsby' ),
			'not_found'                => __( 'No mails found.', 'netsby' ),
			'not_found_in_trash'       => __( 'No mails found in Trash.', 'netsby' ),
			'all_items'                => __( 'All Mails', 'netsby' ),
			'archives'                 => __( 'Mail Archives', 'netsby' ),
			'attributes'               => __( 'Mail Attributes', 'netsby' ),
			'uploaded_to_this_item'    => __( 'Uploaded to this mail', 'netsby' ),
			'menu_name'                => __( 'Mailbox', 'netsby' ),
			'filter_items_list'        => __( 'Filter mails list', 'netsby' ),
			'items_list_navigation'    => __( 'Mails list navigation', 'netsby' ),
			'items_list'               => __( 'Mails list', 'netsby' ),
			'item_published_privately' => __( 'Mail published privately', 'netsby' ),
			'item_updated'             => __( 'Mail updated.', 'netsby' ),
		),
		'description'         => __( 'Stores submitted form data as mails.', 'netsby' ),
		'public'              => false,
		'hierarchical'        => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'show_ui'             => true,
		'show_in_menu'        => ( empty( $mails ) ) ? false : true, // Show menu item only if mails are stored.
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => false,
		'menu_position'       => 0,
		'menu_icon'           => 'dashicons-email-alt',
		'capability_type'     => array( 'mail', 'mails' ),
		'capabilities'        => array(
			'publish_posts' => 'do_not_allow',
			'create_posts'  => 'do_not_allow',
		),
		'map_meta_cap' => false,
		'supports'     => array( 'title' ),
		'taxonomies'   => array( 'mail_tag' ),
		'has_archive'  => false,
		'can_export'   => true,
	) );

	register_taxonomy( 'mail_tag', 'mail', array(
		'description' => __( 'Sorts mails into groups of concerns.', 'netsby' ),
		'public' => false,
		'publicly_queryable' => false,
		'hierarchical' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'show_in_rest' => false,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'capabilities' => array(
			'manage_terms' => 'manage_mail_tags',
			'edit_terms'   => 'manage_mail_tags',
			'delet_terms'  => 'manage_mail_tags',
			'assign_terms' => 'edit_mails',
		),
		'update_count_callback' => '_update_generic_term_count',
	) );
}

function netsby_cpt_trash( $post_id ) {
	// Skip trash for mail post type.
	if ( 'mail' === get_post_type( $post_id ) ) {
		wp_delete_post( $post_id, true );
	}
}
