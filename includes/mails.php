<?php

// Create mail posts from new form submissions.
function netsby_mails_new( $request ) {
	$data    = $request['data'];
	$mail_id = wp_insert_post( array(
		'post_title'     => wp_strip_all_tags( $data['email'] ),
		'post_status'    => 'private',
		'post_type'      => 'mail',
		'comment_status' => 'closed',
		'ping_status'    => 'closed',
		'post_category'  => array(),
	) );

	if ( ! empty( $mail_id ) ) {
		// Set terms separately to mitigate insufficient permission 'assign_terms'.
		wp_set_object_terms( $mail_id, $data['tags'], 'mail_tag' );

		// Insert custom ACF data that is created per term.
		// Use field key instead of name for new values. E.g. field_123456
		foreach( $data['acf'] as $key => $value ) {
			update_field( $key, $value, $mail_id );
		}

		// Send mails.
		netsby_mails_send( $data );
	}

	// Returns 0 on failure.
	return new WP_REST_Response( $mail_id, 200 );
}

// Send mail posts as email.
function netsby_mails_send( $data ) {
	$email = array();

	foreach ( $data['tags'] as $tag ) {
		$term = get_term( $tag, 'mail_tag' );
		$mail = get_field( 'acf_mail', $term ); // Should be a repeater field.

		if ( $mail ) {
			foreach( $mail as $row ) {
				$email[] = array(
					'to'      => $row['to'],
					'subject' => $row['subject'],
				);
			}
		}
	}

	if ( ! empty( $email ) ) {
		$headers = array( 'Content-Type: text/html; charset=UTF-8' );
		$message = '';

		// TODO: Fill $message with $data['acf'] fields.

		foreach ( $email as $recipient ) {
			// From email defaults to wordpress@sitename without www.
			wp_mail( $recipient['to'], $recipient['subject'], $message, $headers );
		}
	}
}
