<?php

// Get metadata for static site.
function netsby_metadata() {
	$mods = NETSBY['mods'];
	$wpml = NETSBY['wpml'];
	$meta = array();

	// Needed to survive normalization of Gatsby entities.
	$meta['wordpress_id'] = 'netsby';

    // Add WPML data for browser redirects.
	if ( ! empty( $wpml ) ) {
		$meta['wpml'] = array(
			'default'     => $wpml['lang_default'],
			'active'      => $wpml['lang_active'],
			'dirs'        => $wpml['lang_dirs'],
			'default_dir' => $mods['lang_default_dir'],
		);
	}

	return new WP_REST_Response( $meta, 200 );
}
