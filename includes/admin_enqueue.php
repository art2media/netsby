<?php

function netsby_admin_enqueue() {
	$build_status = NETSBY['mods']['build_status'];
	$template_uri = NETSBY['uri'];
	$screen       = get_current_screen();
	$localize     = array(
		'ajax_url'      => admin_url( 'admin-ajax.php' ),
		'build_status'  => $build_status,
		'build_text'    => __( 'Publish Changes', 'netsby' ),
		'sending_text'  => __( 'Sending ...', 'netsby' ),
		'building_text' => __( 'Publishing ...', 'netsby' ),
		'loading_text'  => __( 'Loading ...', 'netsby' ),
	);

	// Enqueue on mail post type screen only.
	if ( 'mail' === $screen->post_type ) {
		$localize['mail'] = array(
			'delete_text' => __( 'Delete Permanently' ),
		);

		wp_enqueue_script( 'netsby_mail', $template_uri . '/js/mail.js', array( 'jquery', 'netsby_build' ), '', true );
	}

	wp_enqueue_style( 'netsby_admin_global', $template_uri . '/css/admin-global.css' );

	wp_enqueue_script( 'netsby_build', $template_uri . '/js/build.js', array( 'jquery' ), '', true );
	wp_localize_script( 'netsby_build', 'netsby', $localize );
}
