<?php

function netsby_admin_bar( $wp_admin_bar ) {
	$customize_url = NETSBY['customize_url'];
	$build_url     = NETSBY['mods']['build_url'];
	$build_status  = NETSBY['mods']['build_status'];

	// Set conditional build button args if no build url is set.
	if ( empty( $build_url ) ) {
		$query= array(
			'autofocus[section]' => 'netsby_theme', // Link to theme section in Customizer.
		);

		$title = __( 'Configure', 'netsby' );
		$href  = add_query_arg( $query, $customize_url );
		$class = 'inactive';
	} else {
		$title = __( 'Publish Changes', 'netsby' );
		$href  = $build_url;
		$class = 'active';
	}

	// Add current build status on page load.
	if ( ! empty( $build_status ) ) {
		$class .= ' ' . $build_status;
	}

	// Change building text on page load.
	if ( 'building' === $build_status ) {
		$title = __( 'Publishing ...', 'netsby' );
	}

	// Build Button
	$build_args = array(
		'id'     => 'netsby_build',
		'title'  => $title,
		'href'   => $href,
		'parent' => 'top-secondary',
		'meta'   => array(
			'class' => $class,
		),
	);
	$wp_admin_bar->add_node( $build_args );

	// Refresh Status Button
	if ( ! empty( $build_url ) ) {
		$refresh_icon = '
			<svg version="1.1" id="Ebene_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
				<path d="M0,13l4-5.2L8,13H5.1c0.5,3.4,3.4,6,6.9,6c2.2,0,4.2-1.1,5.5-2.7l1.2,1.6C17.1,19.8,14.7,21,12,21c-4.6,0-8.5-3.5-8.9-8H0z M16,11l4,5.2l4-5.2h-3.1c-0.5-4.5-4.3-8-8.9-8C9.3,3,6.9,4.2,5.2,6.1l1.2,1.6C7.8,6.1,9.8,5,12,5c3.5,0,6.4,2.6,6.9,6H16z"/>
			</svg>
		';
		$refresh_text = '<span class="text">' . __( 'Refresh Status', 'netsby' ) . '</span>';

		$refresh_args = array(
			'id'     => 'netsby_build_status',
			'title'  => $refresh_icon . $refresh_text,
			'href'   => '#',
			'parent' => 'netsby_build',
		);
		$wp_admin_bar->add_node( $refresh_args );
	}

	// Unlock Build Button
	if ( ! empty( $build_url ) ) {
		$unlock_icon = '
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<path d="M12 18c-1.104 0-2-.896-2-2s.896-2 2-2 2 .896 2 2-.896 2-2 2zm0-10c-1.062 0-2.073.211-3 .587v-3.587c0-1.654 1.346-3 3-3s3 1.346 3 3v1h2v-1c0-2.761-2.238-5-5-5-2.763 0-5 2.239-5 5v4.761c-1.827 1.466-3 3.714-3 6.239 0 4.418 3.582 8 8 8s8-3.582 8-8-3.582-8-8-8zm0 14c-3.313 0-6-2.687-6-6s2.687-6 6-6 6 2.687 6 6-2.687 6-6 6z"/>
			</svg>
		';
		$unlock_text = '<span class="text">' . __( 'Unlock', 'netsby' ) . '</span>';

		$unlock_args = array(
			'id'     => 'netsby_build_unlock',
			'title'  => $unlock_icon . $unlock_text,
			'href'   => '#',
			'parent' => 'netsby_build',
		);
		$wp_admin_bar->add_node( $unlock_args );
	}
}
