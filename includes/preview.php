<?php

// Update preview link with correct preview page + id and token hash.
function netsby_preview_link( $preview_link, $post ) {
	$site_url = NETSBY['mods']['site_url'];

	if ( ! empty( $site_url ) ) {
		$post_id    = $post->ID;
		$link_path  = netsby_link_path( $preview_link );
		$meta_key   = 'netsby_preview';
		$token      = netsby_token();
		$meta_value = array(
			'time' => $token['time'],
			'hash' => $token['hash'],
		);

		// Add unique meta key and update if already exists.
		if ( ! add_post_meta( $post_id, $meta_key, $meta_value, true ) ) {
			update_post_meta ( $post_id, $meta_key, $meta_value );
		}

		return untrailingslashit( $site_url ) . $link_path . '?preview=' . $post_id . '&token=' . urlencode( $token['token'] );
	}

	return $preview_link;
}

// Get preview content and return new node values.
function netsby_preview( $request ) {
	$post_id = $request['preview'];
	$node    = $request['node'];
	$post    = get_post( $post_id );

	// Map nodes to post values and save new values.
	foreach ( $node as $key => $value ) {
		switch ( $key ) {
			case 'title':
				$node[ $key ] = $post->post_title;
				break;
			case 'content':
				$node[ $key ] = $post->post_content;
				break;
			case 'excerpt':
				$node[ $key ] = $post->post_excerpt;
				break;
			case 'acf':
				$fields = get_fields( $post_id );

				if ( $fields ) {
					foreach ( $fields as $name => $value ) {
						$node[ $key ][ $name ] = $value;
					}
				}

				break;
		}
	}

	return new WP_REST_Response( $node, 200 );
}
