<?php

$template_dir = NETSBY['dir'];

require_once $template_dir . '/updates/plugin-update-checker.php';

$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/art2media/netsby',
	$template_dir,
	'netsby'
);
