<?php

// Remove a string from the start of another string.
function netsby_str_remove_start( $string, $remove ) {
	if ( substr( $string, 0, strlen( $remove ) ) == $remove ) {
		return substr( $string, strlen( $remove ) );
	}

	return $string;
}

// Get path of link by respecting multisite.
function netsby_link_path( $link ) {
	$blog_path = NETSBY['mu']['blog_path'];

	// Split url into parts.
	// Contains 'scheme', 'host' and 'path'.
	$url = wp_parse_url( $link );

	if ( empty( $blog_path ) ) {
		$path = $url['path'];
	} else {
		// Remove current blog path, if multisite is enabled.
		$path = '/' . netsby_str_remove_start( $url['path'], $blog_path );
	}

	return $path;
}

// Create token to authenticate with Netsby functions.
function netsby_token( $secret = false ) {
	// Get non-cached site urls.
	$site_url = get_theme_mod( 'site_url' );

	if ( empty( 'site_url' ) ) {
		return false;
	}

	$wp_url = get_site_url( null, '', 'https' );
	$pass   = trim( wp_generate_password( 64, true, true ) );
	$time   = current_time( 'Y-m-d' );
	$hash   = wp_hash_password( untrailingslashit( $site_url ) . $pass . $time . untrailingslashit( $wp_url ) );

	if ( $secret ) {
		// Save secret token to theme mods.
		set_theme_mod( 'token_secret', array(
			'time' => $time,
			'hash' => $hash,
		) );

		// Only return unhashed token.
		return $pass;
	} else {
		return array(
			'token' => $pass,
			'time'  => $time,
			'hash'  => $hash,
		);
	}
}

// Check hashed tokens.
function netsby_token_check( $request, $period = false ) {
	$headers      = $request->get_headers();
	$x_from       = $headers['x_from'][0];
	$x_to         = $headers['x_to'][0];
	$x_auth_token = $headers['x_auth_token'][0];

	if (
		! empty( $x_from ) &&
		! empty( $x_to ) &&
		! empty( $x_auth_token )
	) {
		$preview_valid = true;

		// Preview validation.
		if (
			! empty( $request['preview'] ) &&
			! empty( $request['token'] ) &&
			! empty( $request['node'] )
		) {
			$preview       = get_post_meta( $request['preview'], 'netsby_preview', true );
			$time          = $preview['time'];
			$preview_valid = wp_check_password( untrailingslashit( $x_from ) . urldecode( $request['token'] ) . $time . untrailingslashit( $x_to ), $preview['hash'] );
		}

		// Final secret token validation.
		if ( $preview_valid ) {
			$secret       = get_theme_mod( 'token_secret' );
			$secret_valid = wp_check_password( untrailingslashit( $x_from ) . $x_auth_token . $secret['time'] . untrailingslashit( $x_to ), $secret['hash'] );

			if ( $period && $secret_valid ) {
				// Check if token is not older than one day.
				return strtotime($time) > strtotime('-1 day');
			} else {
				return $secret_valid;
			}
		}
	}

	return false;
}
