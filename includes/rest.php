<?php

function netsby_rest_routes() {
	// Provide metadata for Gatsby.
	register_rest_route( 'netsby/v1', 'metadata', array(
		'methods'  => 'GET',
		'callback' => 'netsby_metadata',
	) );
    
	// Register REST API route to receive updated build status.
	// Endpoint looks like: /wp-json/netsby/v1/build/status
	register_rest_route( 'netsby/v1', 'build/status', array(
		'methods'  => 'POST',
		'callback' => 'netsby_build_status',
		'args'     => array(
			'state' => array(
				'required'          => true,
				'validate_callback' => function( $param ) {
					// Netlify does only send a view keywords.
					switch ( $param ) {
						case 'building':
						case 'ready':
						case 'failed':
							return true;
							break;
						default:
							return false;
					}
				}
			),
		),
	) );

	// Route to fetch previews.
	register_rest_route( 'netsby/v1', 'preview', array(
		'methods'  => 'GET',
		'callback' => 'netsby_preview',
		'args'     => array(
			'preview' => array(
				'required'          => true,
				'validate_callback' => function( $param ) {
					// Should be a numeric post id.
					return is_numeric( $param );
				}
			),
			'token' => array(
				'required'          => true,
				'validate_callback' => function( $param ) {
					// Should be a string containing the preview token.
					return is_string( $param );
				}
			),
			'node' => array(
				'required'          => true,
				'validate_callback' => function( $param ) {
					// Should be an array of GraphQL nodes.
					$is_array = is_array( $param );

					// Check if ACF ist activated if ACF nodes are present.
					if (
						$is_array &&
						( ! empty( $param['acf'] ) && is_array( $param['acf'] ) )
					) {
						return function_exists( 'get_fields' );
					}

					return $is_array;
				},
			),
		),
		'permission_callback' => function( $request ) {
			return netsby_token_check( $request, true );
		},
	) );

	// Accept new mails (form submissions).
	register_rest_route( 'netsby/v1', 'mails/new', array(
		'methods'  => 'POST',
		'callback' => 'netsby_mails_new',
		'args'     => array(
			'data' => array(
				'required'          => true,
				'validate_callback' => function( $param ) {
					// Should be an array, ACF should be activated and three keys should exist at least.
					return (
						is_array( $param ) &&
						function_exists( 'update_field' ) &&
						( ! empty( $param['email'] ) && is_string( $param['email'] ) ) &&
						( ! empty( $param['tags'] ) && is_array( $param['tags'] ) ) &&
						( ! empty( $param['acf'] ) && is_array( $param['acf'] ) )
					);
				},
			),
		),
		'permission_callback' => function( $request ) {
			return netsby_token_check( $request );
		},
	) );
}
