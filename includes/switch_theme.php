<?php

// Theme is being deactivated.
function netsby_switch_theme() {
	if ( ! is_child_theme() ) {
		$mods       = NETSBY['mods'];
		$theme      = wp_get_theme();
		$template   = $theme->get( 'Template' );
		$stylesheet = get_stylesheet();

		// Sync theme mods only from/to child theme.
		if ( 'netsby' === $template ) {
			update_option( 'theme_mods_' . $stylesheet, $mods );
		} else {
			// Remove capabilities, if new theme is not a child.
			$admin = get_role( 'administrator' );

			$admin->remove_cap( 'edit_mail' );
			$admin->remove_cap( 'read_mail' );
			$admin->remove_cap( 'delete_mail' );
			$admin->remove_cap( 'edit_mails' );
			$admin->remove_cap( 'edit_others_mails' );
			$admin->remove_cap( 'read_private_mails' );
			$admin->remove_cap( 'manage_mail_tags' );
		}
	}
};

// Theme is being activated.
function netsby_after_switch_theme( $old_name, $old_theme ) {
	if ( ! is_child_theme() ) {
		$old_mods   = get_option( 'theme_mods_' . $old_theme->stylesheet );
		$stylesheet = get_stylesheet();
		$template   = $old_theme->get( 'Template' );

		// Sync theme mods only from/to child theme.
		if ( 'netsby' === $template ) {
			update_option( 'theme_mods_' . $stylesheet, $old_mods );
		} else {
			// Add capabilities, if old theme is not a child.
			$admin = get_role( 'administrator' );

			$admin->add_cap( 'edit_mail' );
			$admin->add_cap( 'read_mail' );
			$admin->add_cap( 'delete_mail' );
			$admin->add_cap( 'edit_mails' );
			$admin->add_cap( 'edit_others_mails' );
			$admin->add_cap( 'read_private_mails' );
			$admin->add_cap( 'manage_mail_tags' );
		}
	}
};
