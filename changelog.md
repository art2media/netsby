# Changelog

## 0.11.2

**Added:**

1. Default language directory setting is now available with Netsby metadata, if WPML is active.

## 0.11.1

**Added:**

1. `wordpress_id` for Netsby's metadata response to survive entitiy normalization.

## 0.11.0

**Added:**

1. Additional metadata that is provided via custom endpoint: `/netsby/v1/metadata`
2. Provide WPML default lang, active languages and directory settings as metadata.

## 0.10.0

**Added:**

1. Theme mod to enable directory for default language, if WPML is active and set to use directories for languages.
2. Permalinks for default language get prepended with the directory language code.

WPML does have a setting to use directory for default language.
But this results in redirects on REST API to the default language folder.
Gatsby is unable to fetch these endpoints.

## 0.9.1

**Changed:**

1. Minor translation changes.
2. Replaced some localization functions with html filter.

## 0.9.0

**Added:**

1. Mail post type (CPT) to store submitted form data.
2. Taxonomy mail_tag to sort mails into groups of concerns (or per formular).
3. Route for new CPT to create new posts and send emails.
4. Disabled mail trash by force delete permanently and renaming the trash button.
5. New CPT capabilities for administrator will be added/removed on theme activation/deactivation.
6. Translations for new post type.

**Changed:**

1. Minor refactor of REST routes.
2. Minor refactor of functions.php file (order or require).

## 0.8.0

**Added:**

1. Experimental preview endpoint for REST API (/netsby/v1/preview).
2. Token generation and validation for both secret (global) and preview.
3. The post id and a preview token (that is valid for one day, will be newly generated on each post edit screen visit) will be appended to the preview url.
4. Request data validation for all routes.

## 0.7.2

**Changed:**

1. Unlock button should not be added to the admin bar, if build url is not set.

## 0.7.1

**Added:**

1. Failed status of site build.

**Changed:**

1. Get build status directly from database during AJAX (not cached at page load) to ensure latest one.
2. Some refactoring.

## 0.7.0

**Added:**

1. Button to unlock build if stuck.
2. Translation for unlock button.

**Changed:**

1. Some CSS changes to sub buttons (refresh and unlock).
2. Minor refactor.

**Removed:**

1. `nopriv` AJAX hooks to prevent unauthenticated users trigger builds and status updates.

## 0.6.2

**Changed:**

1. Translation strings containing "build" were changed to "publish" to clarify things for the editor.
2. Minor comment fix in build.js file.

## 0.6.1

**Changed:**

1. [functions.php] Minor code refactor.
2. Rewritten Readme file.
3. [plugin-update-checker] Update to version 4.5 with some GitLab fixes.

## 0.6.0

**Added:**

1. _Sending_ status text to build button.
2. All Gutenberg core blocks are disabled by default.
3. Theme mods will be synced between parent and child themes after every switch.
4. Customizer control to enable Gutenberg editor – disabled by default.
5. [German] Translations of new strings.

**Fixed:**

1. Language strings failed to load in some cases.

## 0.5.0

**Added:**

1. Status texts for build and refresh buttons.
2. Build button is now disabled during build.
3. [German] Translations of new status texts.

**Changed:**

1. Refactored build.js with new variable names and whitespace.

## 0.4.0

**Added:**

1. String translation into German.

**Changed:**

1. Some strings before translation.

## 0.3.0

**Added:**

1. Update checker for automatic theme updates in WordPress. Uses GitLab tags.

**Fixed:**

1. Customizer autofocus links by adding the WordPress HTML structure including wp_footer() and wp_header() to index.php file. These will be rendered, if no build url is configured.

## 0.2.1

**Changed:**

1. Rewrite of initial constant to get rid of PHP notices (undefined array indexes).
2. Minor CSS fix – the refresh icon should spin in the right direction.

## 0.2.0

**Added:**

1. Build Button to send a Webhook for a new build (including Customizer setting for the Webhook build url).
2. REST API Endpoint to save incoming Webhook build status notifications.
3. Refresh Button to get the newest build status. After clicking on "Build", a script fetches every 5 seconds for a new status, until the build is finished.
4. Rewrite permalinks with static site structure.

## 0.1.0

**Added:**

1. Customizer setting for siteurl (of Gatsby site).

**Changed:**

1. Redirect function to use new theme_mod siteurl.
