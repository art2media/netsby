��    $      <  5   \      0  	   1     ;  	   A  -   K  B   y  	   �     �     �     �     �     �               1     ?     G  
   M     X  Z   n     �     �     �                    )     6  $   B  $   g     �     �  	   �  
   �  "   �  )   �  �       �     �     �  @   �  K        S     h     }     �  	   �     �  	   �      �     �     �     �     	     	  a   9	  )   �	     �	     �	     �	     
      
     -
  	   @
  #   J
  6   n
  
   �
     �
     �
     �
  (   �
  1                                           
                     	                              "              $                       !          #                                          All Mails Build Configure Copy this secret access token somewhere safe: Domain of the static site. Permalinks will be updated accordingly. Edit Mail Enable Gutenberg Filter mails list Loading ... Mail Mail Archives Mail Attributes Mail published privately Mail updated. Mailbox Mails Mails list Mails list navigation More than a theme. Integrates WordPress with Gatsby and Netlify for a convenient workflow. No mails found in Trash. No mails found. Publish Changes Publishing ... Redirect Refresh Status Search Mails Sending ... Sorts mails into groups of concerns. Stores submitted form data as mails. Unlock Uploaded to this mail View Mail View Mails Webhook URL to trigger new builds. [WPML] Use directory for default language Project-Id-Version: Netsby 0.3.0
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/netsby
POT-Creation-Date: 2019-01-30T06:54:07+00:00
PO-Revision-Date: 2019-01-30 07:55+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
X-Domain: netsby
Plural-Forms: nplurals=2; plural=(n != 1);
 Alle Schreiben Bauen Konfigurieren Kopiere diesen geheimen Zugangsschlüssel an einen sicheren Ort: Domain der statischen Website. Permalinks werden entsprechend aktualisiert. Nachricht bearbeiten Gutenberg aktivieren Nachrichtenliste filtern Lade … Nachricht Archiv Attribute Nachricht privat veröffentlicht Nachricht aktualisiert. Postfach Nachrichten Nachrichtenliste Navigation der Nachrichtenliste Mehr als ein Theme. Integriert WordPress mit Gatsby und Netlify für einen komfortablen Workflow. Keine Nachrichten im Papierkorb gefunden. Keine Nachrichten gefunden. Änderungen veröffentlichen Veröffentliche … Weiterleitung Status laden Nachrichten suchen Sende … Sortiert Nachrichten nach Anliegen. Speichert übermittelte Formulardaten als Nachrichten. Entsperren Zu dieser Nachricht hochgeladen Nachricht ansehen Nachrichten ansehen Webhook-URL, um neue Builds auszulösen. [WPML] Verzeichnis für Standardsprache verwenden 