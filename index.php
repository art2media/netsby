<?php
$site_url = NETSBY['mods']['site_url'];

if ( ! empty( $site_url ) ) {
	wp_redirect( $site_url, 301 );

	exit;
}

// Autofocus will not work in Customizer without the following. ?>

<!doctype html>

<html>
	<head>
		<?php wp_head(); ?>
	</head>

	<body>
		<?php wp_footer(); ?>
	</body>
</html>
