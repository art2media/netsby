# Netsby Theme for WordPress

WordPress goes Headless! Netsby connects WordPress to Netlify and Gatsby for a convenient static workflow.

## Features

The theme is in active development.
It does not intend to replace custom field and content type plugins like ACF and Pods.

Gutenberg support has partly built in, but the block editor is disabled by default.
It stays that way, until more documentation for Gutenberg and version 5.8 of ACF with Gutenberg support are available.

### Theme Mod Syncing

WordPress does not sync theme mods between parent and child themes.

Netsby will sync these settings with the child theme on deactivation.
On activation, all settings from the child theme will be synced to the parent.

**Of course:** Syncing triggers only, if the new/old theme is a child theme of Netsby.

### Redirect

Netsby allows to specify the domain path of the static site.

The Frontend of WordPress will redirect to this url at every page visit.
Additionally, all permalinks of post types will be updated to this domain.

### Build

Besides the redirect domain, a Webhook url that triggers a build at Netlify (or every other static site hoster) can be configured in the Customizer.

This enables a Build button in the Backend of WordPress (admin bar) to send a POST request on click.

### REST API

A new route is created, that can be configured in Netlify to send status updates of the build to WordPress.

_wp-json/netsby/v1/build/status_ currently accepts _building_ and _ready_ as status updates and saves these to the database.

### Build Status

The build button will inform about the status of a build with color lamp and text.

- _Build Site_ (green): Build is complete. Ready for new build.
- _Sending ..._ (white blinking): Appears on click, during build request. Followed by ...
- _Building ..._ (yellow): Site is currently being build. Button is disabled.

After sending a build request, the refresh button will fetch the new status every 5 seconds.
This status update button will be disabled if the build is complete (green).
If the admin page is being quit during build (yellow), a manual status update can be triggered by clicking the refresh button.
The status will be updated on every page load, too.

### Gutenberg

As mentioned earlier, the block editor is disabled by default.
However, it can be enabled in the Customizer.

Currently, all core blocks are disabled for later use of custom ACF 5.8 blocks.

## Localization

Netsby will be actively translated into German by the maintainer.
Additional translations are welcome.
