<?php

// Defaults for theme_mods.
$theme_mods = get_theme_mods();
$mods       = array();

$mods['site_url']     = ( empty( $theme_mods['site_url'] ) ) ? '' : $theme_mods['site_url'];
$mods['build_url']    = ( empty( $theme_mods['build_url'] ) ) ? '' : $theme_mods['build_url'];
$mods['build_status'] = ( empty( $theme_mods['build_status'] ) ) ? '' : $theme_mods['build_status'];

$mods['block_editor_enable'] = ( empty( $theme_mods['block_editor_enable'] ) ) ? false : $theme_mods['block_editor_enable'];

$mods['lang_default_dir'] = ( empty( $theme_mods['lang_default_dir'] ) ) ? false : $theme_mods['lang_default_dir'];

// Defaults for multisite.
if ( is_multisite() ) {
	$blog_details = get_blog_details();

	$mu = array(
		'blog_path' => $blog_details->path, // Needed for later permalink rewrite.
	);
} else {
	$mu = array(
		'blog_path' => '',
	);
}

if ( isset( $GLOBALS['sitepress'] ) ) {
	global $sitepress;

	$wpml_settings = $sitepress->get_settings();

	$wpml = array(
		'lang_default' => $sitepress->get_default_language(),
		'lang_active'  => $wpml_settings['active_languages'],
		'lang_current' => ICL_LANGUAGE_CODE,
		'lang_dirs'    => '1' === $wpml_settings['language_negotiation_type'] ? true : false, // String: 1 for directories.
	);
} else {
	$wpml = array();
}

// Constants are slow. Always buffer the values in variables.
define( 'NETSBY', array(
	'wp_site_url'   => get_site_url(),
	'customize_url' => wp_customize_url(),
	'dir'           => get_template_directory(),
	'uri'           => get_template_directory_uri(),
	'mods'          => $mods,
	'mu'            => $mu,
	'wpml'          => $wpml,
) );

$includes_dir = NETSBY['dir'] . '/includes';

// Helper Functions
require_once $includes_dir . '/helpers.php';

// Theme Setup
require_once $includes_dir . '/setup_theme.php';
add_action( 'after_setup_theme', 'netsby_setup_theme' );
add_action( 'admin_notices', 'netsby_token_secret' );

// Switch Theme
require_once $includes_dir . '/switch_theme.php';
add_action( 'switch_theme', 'netsby_switch_theme' );
add_action( 'after_switch_theme', 'netsby_after_switch_theme', 10, 2 );

// Updates
require_once $includes_dir . '/updates.php';

// Customize
require_once $includes_dir . '/customize.php';
add_action( 'customize_register', 'netsby_customize_register' );

// Admin Bar
require_once $includes_dir . '/admin_bar.php';
add_action( 'admin_bar_menu', 'netsby_admin_bar' );

// Admin Enqueue
require_once $includes_dir . '/admin_enqueue.php';
add_action( 'admin_enqueue_scripts', 'netsby_admin_enqueue' );

// Metadata
require_once $includes_dir . '/metadata.php';

// Build
require_once $includes_dir . '/build.php';
add_action( 'wp_ajax_netsby_build', 'netsby_build' );
add_action( 'wp_ajax_netsby_build_status', 'netsby_build_status' );
add_action( 'wp_ajax_netsby_build_unlock', 'netsby_build_unlock' );

// Post Types
require_once $includes_dir . '/post_types.php';
add_filter( 'page_link', 'netsby_permalinks' );
add_filter( 'post_link', 'netsby_permalinks' );
add_filter( 'post_type_link', 'netsby_permalinks' );
add_action( 'init', 'netsby_cpt' );
add_action( 'wp_trash_post', 'netsby_cpt_trash' );

// Preview
require_once $includes_dir . '/preview.php';
add_filter( 'preview_post_link', 'netsby_preview_link', 10, 2 );

// Mails
require_once $includes_dir . '/mails.php';

// REST API
require_once $includes_dir . '/rest.php';
add_action( 'rest_api_init', 'netsby_rest_routes' );

// Gutenberg
$block_editor_enable = ( NETSBY['mods']['block_editor_enable'] ) ? '__return_true' : '__return_false';
add_filter( 'use_block_editor_for_post', $block_editor_enable );

require_once $includes_dir . '/gutenberg.php';
add_filter( 'allowed_block_types', 'netsby_allowed_block_types' );
