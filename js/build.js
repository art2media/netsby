;(function($) {
  var $buildButton = $("#wp-admin-bar-netsby_build.active > a.ab-item")
  var $refreshButton = $(
    "#wp-admin-bar-netsby_build.active #wp-admin-bar-netsby_build_status a.ab-item"
  )
  var $unlockButton = $(
    "#wp-admin-bar-netsby_build.active #wp-admin-bar-netsby_build_unlock a.ab-item"
  )
  var refreshTimeout

  // No need to unlock build if not building.
  if (netsby.build_status !== "building") {
    $unlockButton.hide()
  }

  // Send Webhook for a new build.
  $buildButton.on("click", function(event) {
    event.preventDefault()

    var $self = $(this)
    var $parent = $self.parent()
    var buildUrl = $self.attr("href")

    if (!$parent.hasClass("building")) {
      // Switch status lamp to blinking white.
      $parent.addClass("sending")
      $self.text(netsby.sending_text)

      $.ajax({
        type: "POST",
        url: netsby.ajax_url,
        data: {
          action: "netsby_build",
          build_url: buildUrl
        },
        success: function() {
          // Show unlock button in case something goes wrong.
          $unlockButton.show()

          // Switch status lamp to yellow (building).
          $parent.addClass("building").removeClass("sending ready")
          $self.text(netsby.building_text)

          // Refresh every 5 seconds to get a new build status.
          refreshTimeout = setInterval(function() {
            $refreshButton.trigger("click")
          }, 5000)
        }
      })
    }
  })

  // Fetch updated build status.
  $refreshButton.on("click", function(event) {
    event.preventDefault()

    var $self = $(this)
    var $statusText = $self.children(".text")
    var statusText = $statusText.text()
    var $buildParent = $self.closest("#wp-admin-bar-netsby_build")

    $self.addClass("loading")
    $statusText.text(netsby.loading_text)

    $.ajax({
      type: "POST",
      url: netsby.ajax_url,
      data: {
        action: "netsby_build_status"
      },
      success: function(data) {
        var buildStatus = JSON.parse(data)

        $self.removeClass("loading")
        $statusText.text(statusText)

        if (buildStatus === "ready" || buildStatus === "failed") {
          // Build ready, no unlock necessary.
          $unlockButton.hide()

          // Switch button status.
          $buildParent.addClass(buildStatus).removeClass("building")
          $buildButton.text(netsby.build_text)

          // Stop fetching build status.
          clearInterval(refreshTimeout)
        }
      }
    })
  })

  // Option to unlock if build is stuck.
  $unlockButton.on("click", function(event) {
    event.preventDefault()

    var $self = $(this)
    var $buildParent = $self.closest("#wp-admin-bar-netsby_build")

    $.ajax({
      type: "POST",
      url: netsby.ajax_url,
      data: {
        action: "netsby_build_unlock"
      },
      success: function() {
        // Build unlocked, no need to unlock again.
        $self.hide()

        // Switch build status to ready.
        $buildParent.addClass("ready").removeClass("building sending")
        $buildButton.text(netsby.build_text)
      }
    })
  })
})(jQuery)
